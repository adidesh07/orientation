# **Git Summary**
- Git is an open source version control system designed to handle projects and manage code.
- It makes collaboration with teammates super simple.
- Git lets one easily keep track of every revision their team make during the development of their software.

![](https://res.cloudinary.com/practicaldev/image/fetch/s--bjpVKHPe--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/8ogqpfkvqqpyfbs3w6p7.png)

## Basic Terminology
- **Repository**: A repository is the collection of files and folders that you’re using git to track.
- **Commit**: Commit is like saving a project locally before pushing it.
- **Push**: Pushing is essentially syncing your commits to GitLab.
- **Branch**: These are separate instances of the code that is different from the main codebase.
- **Merge**: Merging is just integrating two branches together.
- **Clone**: It takes the entire online repository and makes an exact copy of it on your local machine.
- **Fork**: Instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

## Getting started
#### Installing Git:
- **Linux**: `sudo apt-get install git`
- **Windows**: Download the [git installer](https://git-scm.com/download/win) and run it first. Give git some basic details like name and email address.
    - `git config --global user.name "Your Name"`
    - `git config --global user.email email@example.com`

#### Git Internals:
3 main states of files:
- **Modified**: Changed the file but not committed
- **Staged**: Marked the changes but not committed yet
- **Committed**: Changes have been saved to repository

Workspace is the tree of repo in which you make all the changes through editor. Staging is the repo in which all the staged files are saved. Local Repository is where all the committed files are saved. Remote Repository is the copy of local repo on some server, changes made on loacl repo doesn't affect this repo.

## Workflow:
![](https://business-science.github.io/shiny-production-with-aws-book/img/09_git_cli/git_commands.png)


## Basic Commands:
![](https://rubygarage.s3.amazonaws.com/uploads/article_image/file/599/git-cheatsheet-5.jpg)


## Reference Links
1. [Official Git Documentation](https://git-scm.com/doc)
2. [Git Tutorial](https://git-scm.com/docs/gittutorial)
3. [Quick Git Reference](https://git-scm.com/docs)